<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Begin page content -->
<main role="main" class="container">
	<section class="jumbotron text-center">
		<div class="container">
			<h1 class="jumbotron-heading">Home</h1>
			<p>Home alaihom gambreng.</p>
		</div>
	</section>
	<section>
		<div class="row">
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">
						Pra UTS
					</div>
					<div class="card-body text-left">
						<p class="card-text">Materi sebelum UTS</p>
						<div class="row">
							<table class="table table-hover table-striped">
								<thead>
									<tr>
										<th scope="col" class="text-center">Minggu</th>
										<th scope="col">Topik</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row" class="text-center">1</th>
										<td>Intro &amp; Instalasi</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">2</th>
										<td>Pengenalan MVC Bag. 1</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">3</th>
										<td>Pengenalan MVC Bag. 2</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">4</th>
										<td>Pengenalan MVC Bag. 3</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">5</th>
										<td>CRUD &amp; File Upload</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">6</th>
										<td>CRUD &amp; Form Validasi</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">7</th>
										<td>CRUD &amp; Datagrid</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">8</th>
										<td>CRUD, Filtering, &amp; Pagination</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">9</th>
										<td>UTS</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">
						Pra UAS
					</div>
					<div class="card-body text-left">
						<p class="card-text">Materi sebelum UAS</p>
						<div class="row">
							<table class="table table-hover table-striped">
								<thead>
									<tr>
										<th scope="col" class="text-center">Minggu</th>
										<th scope="col">Topik</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row" class="text-center">10</th>
										<td>Security, Login &amp; Logout</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">11</th>
										<td>User Management</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">12</th>
										<td>Role Based Access Management Bag. 1</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">13</th>
										<td>Role Based Access Management Bag. 2</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">14</th>
										<td>Role Based Access Management Bag. 3</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">15</th>
										<td>Kuis</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">16</th>
										<td>Presentasi Tugas Besar</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">17</th>
										<td>UAS</td>
									</tr>
									<tr>
										<th scope="row" class="text-center">18</th>
										<td>Remidi</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>