# CI3-Course

Repo ini berisi source code blog sederhana yang dibangun menggunakan framework CodeIgniter 3, Bootstrap, serta javascript library jQuery.


### Instalasi

1. Buat sebuah database bernama `codeigniter` lalu jalankan skrip SQL `ci3-course.sql`.
2. Buatlah file `config.php` dan `database.php` di dalam folder `application/config/development`. Anda bisa menyalin dari file original yang ada dan sesuaikan setting dengan environment PC Anda.
3. Done.

### Plugin & Library

Daftar plugin dan library yang dipakai dalam projek primitif ini:

| Plugin |  Info | Jenis | Sumber |
| ------ | ------ | ------ | ------ |
| Holder | client side image placeholders | JS |[http://holderjs.com](http://holderjs.com) |

Lisensi
----

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)](http://www.wtfpl.net/)

